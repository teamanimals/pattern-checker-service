/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages.uploader.components;

/**
 *
 * @author asus
 */

import java.io.IOException;
import pages.uploader.service.impl.UploaderServiceImpl;
import pages.uploader.service.UploaderService;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.alem.semantics.links.rest.client.dao.impl.TemplatesDaoClient;
import kz.alem.semantics.links.rest.client.dao.impl.TopicDaoClient;
import kz.alem.semantics.sql.orm.dao.TemplatesDao;
import kz.alem.semantics.sql.orm.dao.TopicDao;
import kz.alem.semantics.sql.orm.model.Template;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.scheduling.annotation.EnableScheduling;
import static pages.uploader.components.PatternChecker.templateMap;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Component
@EnableScheduling


public class PatternChecker extends Thread{
        static Map<String, Template> templateMap;
        TemplatesDao templateDao;
        
        
        
        UploaderService upService;
        String domain="",pattern="";
        
	@Override
	public void run() {
          
                try {
                    
                    String url ="http://nur.kz/dic";
                    templateDao = new TemplatesDaoClient("http://52.18.29.33:7092");
                    initializeTemplates();
                    List<Template> templateList = templateDao.getAll();
                    
                    upService = new UploaderServiceImpl(url, templateMap);
                    try {
                        domain = upService.getDomainName(url);
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(PatternChecker.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    System.out.println(domain);
                
                            
                    pattern=upService.getURLPatternByUrl(domain);
                    System.out.println(pattern);
                    
                    
                    Document doc = Jsoup.connect("https://"+domain).get();
                    Elements links = doc.select("a[href]");
                    
                    print("\nLinks: (%d)", links.size());
                    
                    
                    for (Element link : links)
                    {
                       // print(" * a: <%s>  (%s)", link.attr("abs:href"), trim(link.text(), 35));
                        //System.out.println(link.attr("abs:href"));
                        String url2=link.attr("abs:href");
                        System.out.println(link.attr("abs:href"));
                        System.out.println(IsMatch(url2,pattern));
                        
                    }
                    
                    
                    
                    
                    
                    
                } catch (IOException ex) {
                    Logger.getLogger(PatternChecker.class.getName()).log(Level.SEVERE, null, ex);
                }
                
           
            
                
          
             	
	}
    public void initializeTemplates() {
        templateMap = new HashMap<>();
        List<Template> templateList = templateDao.getAll();
        loadTemplatesToMap(templateList);
    }
    public void loadTemplatesToMap(List<Template> templateList){
        for (Template template : templateList) {
            String domain = template.getDomain();
            if (domain == null)
                continue;
            templateMap.put(domain.trim(), template);
        }
    }
    
    private static void print(String msg, Object... args) 
    {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) 
    {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;
    }
    
     private static boolean IsMatch(String s, String pattern) {
        try{
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } 
        catch(RuntimeException e) {
            return false;
        }       
    } 
    
  

}