/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages.uploader.util;

import java.util.List;

/**
 *
 * @author rafael
 */
public class URL {
    private String url;
    private String statusCode;
    private ErrorMsg errorMsg;
    private List<String> outlinks;
    private String id;

    public ErrorMsg getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(ErrorMsg errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public URL(String _url, List<String> _outlinks, String _statusCode){
        this.url = _url;
        this.outlinks =_outlinks;
        this.statusCode = _statusCode;
    }
    public URL(String _url, List<String> _outlinks, String _statusCode, ErrorMsg _errorMessage){
        this.url = _url;
        this.outlinks =_outlinks;
        this.statusCode = _statusCode;
        this.errorMsg = _errorMessage;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ErrorMsg getErrorMessage() {
        return errorMsg;
    }

    public void setErrorMessage(ErrorMsg errorMessage) {
        this.errorMsg = errorMessage;
    }

    public List<String> getOutlinks() {
        return outlinks;
    }

    public void setOutlinks(List<String> outlinks) {
        this.outlinks = outlinks;
    }
    public class ErrorMsg{
            String code;
            boolean connect;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public boolean isConnect() {
            return connect;
        }

        public void setConnect(boolean connect) {
            this.connect = connect;
        }
    }
}
