/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages.uploader.service;

import java.net.URISyntaxException;
import pages.uploader.util.URL;

/**
 *
 * @author rafael
 */
public interface UploaderService {
    public String getDomainName(String url)throws URISyntaxException;
    public String getURLPatternByUrl(String url);
}
