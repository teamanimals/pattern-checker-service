/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages.uploader.service.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import pages.uploader.service.UploaderService;
import pages.uploader.util.URL;
import kz.alem.semantics.linkdb.orm.dao.PageDao;
import kz.alem.semantics.linkdb.orm.model.Page;
import kz.alem.semantics.linkdb.orm.model.PageStatus;
import kz.alem.semantics.linkdb.orm.model.PageType;
import kz.alem.semantics.links.rest.client.dao.impl.PageDaoClient;
import kz.alem.semantics.sql.orm.model.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author rafael
 */

public class UploaderServiceImpl implements UploaderService{
    Logger logger = LoggerFactory.getLogger(UploaderServiceImpl.class);
    PageDao pageDao;
    String noPattern = "noPattern";
    static Map<String, Template> templateMap;
    public UploaderServiceImpl(String linksRestUrl,Map<String, Template> _templateMap){
        pageDao = new PageDaoClient(linksRestUrl);
        templateMap = _templateMap;
    }
    
    @Override
    public String getDomainName(String url)throws URISyntaxException{
        String domain ="";
        try{
            URI uri = null;
            if(url.contains("https"))
                uri = new URI(url.substring(0, 8+url.substring(8).indexOf("/")));
            else uri = new URI(url.substring(0, 7+url.substring(7).indexOf("/")));
            domain = uri.getHost();
            if(domain!=null){
                domain = domain.startsWith("www.") ? domain.substring(4) : domain;
                domain = domain.trim();
            }
        }
        catch(Exception ex){
            return null;
        }
        return domain;
    }
    public PageStatus getPageStatusByPattern(String pattern, String link){
        if(pattern!=null){
            if((isPageMatchingURLPattern(link, pattern) || pattern.equals(noPattern)) && !link.contains(".pdf") && !link.contains(".doc")){
               return PageStatus.NEW;
            }
        } 
        return PageStatus.REJECTED;
    }
            
    
    public boolean isPageMatchingURLPattern(String url, String pattern){
        if (url.trim().matches(pattern))
            return true;
        return false;
    }
    @Override
    public String getURLPatternByUrl(String domain){
        Template template = null;
        for(String dom : domain.split("\\.")){
            if (templateMap.containsKey(domain.substring(domain.indexOf(dom)))) {
                template =  templateMap.get(domain.substring(domain.indexOf(dom)));
                break;
            }
        }
        if (template != null) {
            String urlPatternString = template.getUrlPattern();
            if(urlPatternString != null){
                if(!urlPatternString.isEmpty())
                    return urlPatternString.trim();
            }
            return noPattern;
        }
        return null;
    }
}
