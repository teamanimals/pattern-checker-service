/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;


import pages.uploader.components.PatternChecker;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author rafael
 */
@Configuration
@SpringBootApplication

@EnableScheduling
public class Main{
    
    public static void main(String args[]) throws IOException,TimeoutException{
        ConfigurableApplicationContext context = SpringApplication.run(Main.class, args);
        PatternChecker npu = context.getBean(PatternChecker.class);
        npu.start();  
    }
}
