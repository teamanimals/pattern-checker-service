/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static com.fasterxml.jackson.databind.jsonFormatVisitors.JsonValueFormat.URI;
import static com.google.gson.internal.bind.TypeAdapters.URI;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Zhansaya_PC
 */
public class RegEx {
    public static void main(String[] args) throws MalformedURLException {
    String s1="https://www.youtube.com";
    String s2="https://www.youtube.com/watch?v=qgckySJtN1g";
    String s3="http://stackoverflow.com/questions/163360/regular-expression-to-match-urls-in-java";
    String s4="https://mail.google.com/mail/u/0/#inbox";
    String s5="https://vlast.kz/novosti/16165-na-nauryz-almatincy-smogut-besplatno-posetit-sportivnye-zaly.html";
    String s6="https://www.instagram.com/hntaj/";
    String s7="http://www.w3schools.com/html/default.asp";
    String s8="https://www.coursera.org/specializations/netpingtai-ruanjian-kaifa";
    String s9="https://support.rackspace.com/how-to/introducing-the-rackspace-cloud-control-panel/";
    String s10="https://habrahabr.ru/company/it_people/blog/278973/";
    
    String e1="(.*)//(.*)[.](com)";
    String e2="(.*)//(.*)[.](com)[/](.*)";
    String e3="(.*)//(.*)[.](com)[/]((.*)[/](.*)[/](.*)[-](.*)[-](.*)[-](.*)[-](.*)[-](.*)[-](.*))";
    String e4="(.*)//(.*)[.](com)[/](.*)[/]\\d[/](.*)";
    String e5="(.*)//(.*)[.](kz)[/]((.+)[/]\\d+(.+)[.](html)|(.+)[/]\\d+[/])";
    String e6="(.*)//(.*)[.](com)[/](.*)[/]";
    String e7="(.*)//(.*)[.](com)[/](html)[/](.*)[.](.*)";
    String e8="(.*)//(.*)[.](org)[/](.*)[/](.*)[-](.*)[-](.*)";
    String e9="(.*)//(.*)[.](com)[/](.*)[-](.*)[/](.*)[-](.*)[-](.*)[-](.*)[-](.*)[-](.*)[/]";
    String e10="(.*)//(.*)[.](ru)[/](.*)[/](.*)[_](.*)[/](.*)[/](.*)";
    
       /* System.out.println(IsMatch(s1, e1));
        System.out.println(IsMatch(s2, e2));
        System.out.println(IsMatch(s3, e3));
        System.out.println(IsMatch(s4, e4));
        System.out.println(IsMatch(s5, e5));
        System.out.println(IsMatch(s6, e6)); 
        System.out.println(IsMatch(s7, e7));
        System.out.println(IsMatch(s8, e8));
        System.out.println(IsMatch(s9, e9));
        System.out.println(IsMatch(s10, e10));*/
        //System.out.println(getDomainName(s7));
        
        
    }
    
   
    
    private static boolean IsMatch(String s, String pattern) {
        try{
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } 
        catch(RuntimeException e) {
            return false;
        }       
    } 
    
    /*public static String getDomainName(String url) throws MalformedURLException{
    if(!url.startsWith("http") && !url.startsWith("https")){
         url = "http://" + url;
    }        
    URL netUrl = new URL(url);
    String host = netUrl.getHost();
    if(host.startsWith("www")){
        host = host.substring("www".length()+1);
    }
    return host;
}*/
}
